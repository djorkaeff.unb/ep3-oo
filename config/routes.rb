Rails.application.routes.draw do
  devise_for :forms, :controllers => { registrations: 'registrations' }
  root to: redirect('/posts')
  resources :posts
  resources :forms
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
