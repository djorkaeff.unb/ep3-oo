class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :photo
      t.string :title
      t.text :description
      t.integer :id_autor

      t.timestamps
    end
  end
end
