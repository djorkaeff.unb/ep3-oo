class RemoveNumPostsFromForms < ActiveRecord::Migration[5.1]
  def change
    remove_column :forms, :num_posts, :integer
  end
end
