class CreateForms < ActiveRecord::Migration[5.1]
  def change
    create_table :forms do |t|
      t.boolean :admin

      t.timestamps
    end
  end
end
