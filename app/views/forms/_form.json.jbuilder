json.extract! form, :id, :admin, :created_at, :updated_at
json.url form_url(form, format: :json)
