class Post < ApplicationRecord
	mount_uploader :photo, PhotoUploader

	def next
    	Post.where("id > ?", id).order(id: :asc).limit(1).first
	end

	def previous
		Post.where("id < ?", id).order(id: :desc).limit(1).first
	end

	def last_value
		return Post.order(id: :desc).limit(1).last
	end

	def first_value
		return Post.order(id: :asc).limit(1).first
	end
end
